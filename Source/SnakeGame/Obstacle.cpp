// Fill out your copyright notice in the Description page of Project Settings.


#include "Obstacle.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "Food.h"
#include "SnakeBase.h"

// Sets default values
AObstacle::AObstacle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//initialize a mesh�omponent of the type UStaticMeshComponent
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	//setting a collision (overlap without physics)
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	// we choose the answer on all channels in case of a collision
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

	
}

// Called when the game starts or when spawned
void AObstacle::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AObstacle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//Called when the snake crashes into this obstacle
//and the snake loses lives (and tail)
void AObstacle::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		Snake->Destroy();

	}
}


