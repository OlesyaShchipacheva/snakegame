// Fill out your copyright notice in the Description page of Project Settings.


#include "Spawner.h"
#include "Food.h"
#include "Bonus.h"
#include "Interactable.h"

// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ChanceBonus = 20;
	NumberSpawnedFood = 5;
	food = 0;
	IsExistBonus = false;
}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	
	//������� ��� ��� �����
	bool IsCreated = false;
	while (!IsCreated)
		IsCreated = SpawnElement();
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FVector ASpawner::GetRandomLocation()
{
	FVector RandomLocation;
	RandomLocation.X = FMath::FRandRange(GetComponentsBoundingBox().Min.X, GetComponentsBoundingBox().Max.X);
	RandomLocation.Y = FMath::FRandRange(GetComponentsBoundingBox().Min.Y, GetComponentsBoundingBox().Max.Y);
	RandomLocation.Z = 0;
	return RandomLocation;
}


//spawn food with random coordinates within the parent actor
bool ASpawner::SpawnElement()
{
	int RandomValue = rand() % 100;
	//UE_LOG(LogTemp, Warning, TEXT("Random number: %d"), RandomValue);
	
	AActor* SpawnedActor;

	FTransform NewTransform(GetRandomLocation());
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

	if ((RandomValue < ChanceBonus) && !IsExistBonus)
	{
		SpawnedActor = GetWorld()->SpawnActor<AActor>(BonusActorClass, NewTransform, SpawnParams);
		if (IsValid(SpawnedActor))
			IsExistBonus = true;
	}
	NewTransform = FTransform(GetRandomLocation());
	SpawnedActor = GetWorld()->SpawnActor<AActor>(FoodActorClass, NewTransform, SpawnParams);
	
	if (!IsValid(SpawnedActor))
		return false;
	SpawnedActor->OnDestroyed.AddDynamic(this, &ASpawner::InteractDestoy);	
	food++;
	return true;
}

void ASpawner::InteractDestoy(AActor* DestroyedActor)
{
	UE_LOG(LogTemp, Warning, TEXT("Destroyed Food"));
	auto Food = Cast<AFood>(DestroyedActor);

	if (IsValid(Food)) 
	{
		NumberSpawnedFood--;
		food--;
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("%d"), NumberSpawnedFood));
		if (NumberSpawnedFood > 0)
			SpawnElement();
		else
			FoodIsOver();
	}
	else
		IsExistBonus = false;
	if(food==0)
		SpawnElement();
}

void ASpawner::FoodIsOver_Implementation()
{

}



