// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Spawner.generated.h"

class AFood;
class ABonus;
class UInteractable;

UCLASS()
class SNAKEGAME_API ASpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawner();
	int food;
	//the chance of getting a bonus in %
	//takes a value from 0 to 100. By default 20%
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	int ChanceBonus;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodActorClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABonus> BonusActorClass;

	//Sets how much food you need to eat to win.
	//Decreases by 1 with each eating. Default value 5
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	int NumberSpawnedFood;

	bool IsExistBonus;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	 

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION(BlueprintCallable)
	bool SpawnElement();
	
	UFUNCTION()
	void InteractDestoy(AActor* DestroyedActor);

	// �alled when all the food is eaten
	UFUNCTION(BlueprintNativeEvent)
	void FoodIsOver();
	void FoodIsOver_Implementation();

	FVector GetRandomLocation();
};
