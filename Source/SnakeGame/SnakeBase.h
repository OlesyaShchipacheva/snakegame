// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"
class ASnakeElementBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;
	
	//Scale of the snake element.
	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
	EMovementDirection LastMoveDirection;

	//The actual speed of the snake, taking into account acceleration.
	UPROPERTY(BlueprintReadOnly)
	float MovementSpeed;

	//The speed of the snake at the start of the game. 
	//Sets the tick interval of the actor in seconds
	UPROPERTY(EditDefaultsOnly)
	float InitialMovementSpeed;

	//This value changes the speed of movement when eating food or a bonus
	//Default value 0.2
	UPROPERTY(EditDefaultsOnly)
	float IncrementSpeedMovement;

	//Maximum permissible speed of the snake in seconds
	UPROPERTY(EditDefaultsOnly)
	float MaxMovementSpeed;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	//snake tail lengthens when eating food
	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementsNum = 1);
	
	//every tick the snake is crawling
	UFUNCTION(BlueprintCallable)
	void Move();

	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

	//The snake accelerates by 0.2 sec
	UFUNCTION(BlueprintCallable)
	void Acceleration();

	//The snake decelerates by 0.2 sec
	UFUNCTION(BlueprintCallable)
	void Deceleration();

};
