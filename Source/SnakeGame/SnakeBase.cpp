// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 60.f;
	LastMoveDirection = EMovementDirection::RIGHT;
	InitialMovementSpeed = 1.f;
	MovementSpeed = 1.f;
	IncrementSpeedMovement = 0.2;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();

	MovementSpeed = InitialMovementSpeed;

	SetActorTickInterval(MovementSpeed);
	
	AddSnakeElement(5);
}
// Called when the game ends or when destroyed
void ASnakeBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	//UE_LOG(LogTemp, Warning, TEXT("Snake destroyed"));
}
// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Move();
}

//snake tail lengthens when eating food
void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (auto i = 0; i < ElementsNum; i++) 
	{
		FVector NewLocation(ForceInitToZero);
		if(SnakeElements.Num()>0)
			NewLocation = SnakeElements.Last()->GetActorLocation();

		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElementIndex = SnakeElements.Add(NewSnakeElem);

		if (ElementIndex == 0)
			NewSnakeElem->SetFirstElementType();
	}
}

//Every tick the snake is crawling
void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y -= ElementSize;
		break;
	}

	// collision is disabled
	SnakeElements[0]->ToggleCollision();

	//the snake is crawling
	for (auto i = SnakeElements.Num() - 1; i > 0; i--) 
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i-1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	SnakeElements[0]->AddActorWorldOffset(MovementVector);

	// collision is enabled
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if(IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;

		IInteractable* IneractableInterface = Cast<IInteractable>(Other);
		if (IneractableInterface)
		{
			IneractableInterface->Interact(this, bIsFirst);
		}
	}
}

//The snake accelerates by 0.2 sec
void ASnakeBase::Acceleration()
{
	float NewSpeed = MovementSpeed - IncrementSpeedMovement;
	
	if (NewSpeed < MaxMovementSpeed)
		MovementSpeed = MaxMovementSpeed;
	else
		MovementSpeed = NewSpeed;
	
	SetActorTickInterval(MovementSpeed);
}

//The snake decelerates by 0.2 sec
void ASnakeBase::Deceleration()
{
	float NewSpeed = MovementSpeed + IncrementSpeedMovement;
	
	if (NewSpeed > InitialMovementSpeed)
		MovementSpeed = InitialMovementSpeed;
	else
		MovementSpeed = NewSpeed;

	SetActorTickInterval(MovementSpeed);
}