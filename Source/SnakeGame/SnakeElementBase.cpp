// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElementBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"

// Sets default values
ASnakeElementBase::ASnakeElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//initialize a mesh�omponent of the type UStaticMeshComponent
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	//setting a collision (overlap without physics)
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	// we choose the answer on all channels in case of a collision
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();
}
void ASnakeElementBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}

// Called every frame
void ASnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
// Called when add a snake head
void ASnakeElementBase::SetFirstElementType_Implementation()
{ 
	//binding the beginOverlap event of the mesh component and the snake
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
}
//Called when a snake crashes into its tail
void ASnakeElementBase::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		Snake->Destroy();
	}
}
//Called when the snake's head hits something
void ASnakeElementBase::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweet,
	const FHitResult& SweetResult)
{
	if (IsValid(SnakeOwner))
	{
		SnakeOwner->SnakeElementOverlap(this, OtherActor);
		//UE_LOG(LogTemp, Warning, TEXT("Snake overlap"));
	}
}
// �alled when adding snake elements to temporarily disable and enable collisions
void ASnakeElementBase::ToggleCollision()
{
	if (MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision) {
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		//UE_LOG(LogTemp, Warning, TEXT("Collision on"));
	}
	else {
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		//UE_LOG(LogTemp, Warning, TEXT("Collision off"));
	}
}
